import os

from flask import Flask, Blueprint
from flask_mongoengine import MongoEngine

from .config import config


db = MongoEngine()
main = Blueprint('catalogue', __name__, url_prefix='/api')
from . import views

def create_app(config_name):
    app = Flask(__name__)

    app.config.from_object(config[config_name])

    app.register_blueprint(main)

    db.init_app(app)

    return app
