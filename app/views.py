from flask import request

from . import main


@main.route('/', methods=('GET',))
def index():
    return '<h1>Hey!</h1>'
