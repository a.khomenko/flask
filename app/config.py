class DevConfig:

    DEBUG = True
    MONGODB_DB = 'av_raw'
    MONGODB_HOST = 'localhost'
    MONGODB_PORT = 27017

config = {
    'default': DevConfig,
    'development': DevConfig
}
